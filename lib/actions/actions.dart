import 'package:redux_lecture_practice/dto/image_dto.dart';

class RequestAction{

  RequestAction();
}

class SaveDataAction{
  final List<ImageDto> data;
  SaveDataAction({this.data});
}

class SetInitDataAction{
  final List<ImageDto> data;
  SetInitDataAction({this.data,});
}
class EmptyAction  {
  EmptyAction();
}