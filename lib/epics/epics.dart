import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';

import 'package:redux_lecture_practice/repos/image_repository.dart';
import 'package:redux_lecture_practice/models/app_state.dart';
import 'package:redux_lecture_practice/dto/image_dto.dart';
import 'package:redux_lecture_practice/actions/actions.dart';

class PhotoEpics {
  static final indexEpic = combineEpics<AppState>([
    requestEpic,
    setInitDataEpic,
  ]);
  static Stream<dynamic> requestEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    print('EPIC');
    return actions.whereType<RequestAction>().switchMap((action) {
      return Stream.fromFuture(
        ImageRepository.instance.getPhotos().then((List<ImageDto> imageDto) {
          if (imageDto == null) {
            return EmptyAction();
          }
          return SetInitDataAction(
            data: imageDto,
          );
        }),
      );
    });
  }
  static Stream<dynamic> setInitDataEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    print('setInitDataEpic');
    return actions.whereType<SetInitDataAction>().switchMap((action) {
      return Stream.fromIterable([
        SaveDataAction(data: action.data),
      ]);
    });
  }
}
