import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_lecture_practice/models/image_page_view_model.dart';

import 'models/app_state.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ImagePageViewModel>(
      converter: ImagePageViewModel.fromStore,
      onInitialBuild: (ImagePageViewModel viewModel) => viewModel.getRequest(),
      builder: (BuildContext context, ImagePageViewModel viewModel) {
        return Scaffold(
          backgroundColor: Colors.cyan,
          appBar: AppBar(
            title: Text(
              'Flutter Redux Fetcher',
            ),
          ),
          body: ListView.builder(
            itemCount: viewModel.urls.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.all(30.0),
                child: Card(
                  color: Colors.orangeAccent,
                  elevation: 5,

                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: Image(
                        image: NetworkImage(viewModel.urls[index]),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
