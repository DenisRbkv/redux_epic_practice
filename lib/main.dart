import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

import 'models/app_state.dart';
import 'home_page.dart';
import 'models/photo_state.dart';
import 'epics/epics.dart';



void main() {
  Store store = Store<AppState>(
    AppState.reducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
      EpicMiddleware(AppState.getAppEpics),
    ],
  );


  runApp(MyApp(store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp(this.store);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Redux Demo',
        home: HomePage(),
      ),
    );
  }
}