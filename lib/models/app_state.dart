import 'package:flutter/material.dart';
import 'package:redux_epics/redux_epics.dart';
import 'dart:collection';

import 'package:redux_lecture_practice/models/app_state.dart';
import 'package:redux_lecture_practice/epics/epics.dart';
import 'package:redux_lecture_practice/actions/actions.dart';
import 'package:redux_lecture_practice/models/photo_state.dart';
import 'package:redux_lecture_practice/selectors/photo_selector.dart';

@immutable
class AppState {
  final PhotoState photoState;

  AppState({
    @required this.photoState,
  });

  static final getAppEpics = combineEpics<AppState>([
    PhotoEpics.indexEpic,
  ]);

  factory AppState.initial() => AppState(
        photoState: PhotoState.initial(),
      );

  static AppState reducer(AppState state, dynamic action) {
    return AppState(
      photoState: state.photoState.reducer(action),
    );
  }
}

class Reducer<T> {
  final String TAG = '[Reducer<$T>]';
  HashMap<dynamic, T Function(dynamic)> actions;

  Reducer({
    @required this.actions,
  }) {
    actions.forEach((key, value) {
      if (value == null) throw ('All Functions must be initialize');
    });
  }

  T updateState(dynamic action, T state) {
    if (actions.containsKey(action.runtimeType)) {
      return actions[action.runtimeType](action);
    }
    return state;
  }
}