import 'package:redux/redux.dart';
import 'package:redux_lecture_practice/models/app_state.dart';
import 'package:redux_lecture_practice/selectors/photo_selector.dart';

class ImagePageViewModel {
  final List<String> urls;
  void Function() getRequest;

  ImagePageViewModel({this.urls, this.getRequest});

  static ImagePageViewModel fromStore(Store<AppState> store) {
    return ImagePageViewModel(
      urls: PhotoSelector.getUrls(store),
      getRequest: PhotoSelector.getRequest(store),
    );
  }
}
