import 'dart:collection';

import 'package:redux_lecture_practice/dto/image_dto.dart';
import 'package:redux_lecture_practice/models/app_state.dart';
import 'package:redux_lecture_practice/actions/actions.dart';

class PhotoState {
  final List<String> imageUrls;

  PhotoState (this.imageUrls);

  factory PhotoState.initial() => PhotoState (List());


  PhotoState reducer(dynamic action) {
    return Reducer<PhotoState>(
      actions: HashMap.from({
        SaveDataAction: (dynamic action) => saveData((action as SaveDataAction).data),
      }),
    ).updateState(action, this);
  }

  PhotoState saveData(List<ImageDto> data){
    List<String> urls = [];
    data.forEach((el) {
      urls.add(el.imageUrls.regular);
    });
    return PhotoState(urls);
  }

}