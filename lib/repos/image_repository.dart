import 'package:redux_lecture_practice/dto/image_dto.dart';
import 'package:redux_lecture_practice/service/photo_service.dart';


class ImageRepository {
  ImageRepository._privateConstructor();

  static final ImageRepository _instance = ImageRepository._privateConstructor();

  static ImageRepository get instance => _instance;

  Future<List<ImageDto>> getPhotos() async {
    print('REPO');
    return await PhotoService.instance.getData();
  }
}