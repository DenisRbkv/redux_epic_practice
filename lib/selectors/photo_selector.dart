import 'package:redux_lecture_practice/models/app_state.dart';
import 'package:redux_lecture_practice/actions/actions.dart';
import 'package:redux/redux.dart';

class PhotoSelector {

  static void Function() getRequest(Store<AppState> store) {
    return () => store.dispatch(RequestAction());
  }

  static List<String> getUrls(Store<AppState> store) {
    return  store.state.photoState.imageUrls;
  }

}