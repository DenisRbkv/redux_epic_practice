import 'package:flutter/foundation.dart';
import 'package:redux_lecture_practice/res/const.dart';
import 'package:redux_lecture_practice/dto/image_dto.dart';

import 'package:redux_lecture_practice/helpers/http_helper.dart';

class PhotoService {
  PhotoService._privateConstructor();

  static final PhotoService _instance = PhotoService._privateConstructor();

  static PhotoService get instance => _instance;

  Future<List<ImageDto>> getData() async {
    try {
      List<dynamic> response =
      await HttpHelper.instance.request(IMAGE_URL, HttpType.get);
      print('FETCH OK');
      final List<ImageDto> data = List();

      for (var item in response) {
        data.add(ImageDto.fromJson(item));
      }
      return data;
    } catch (error) {
      print(error);
    }
  }
}
